package goeuro;

import java.util.List;

import goeuro.common.Location;
import goeuro.out.CSVOutput;
import goeuro.remoteapi.GoEuroAPI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Processor {
	private static final Logger logger = LogManager.getLogger(Processor.class.getName());
	
	
	public Processor() {
		
	}
	
	public static void process( String input){
		try {
			logger.info("Connectiong to remote API");
			List<Location> reply = GoEuroAPI.getReply(input);
			logger.info("Finished getting reply from remote API");
			
			if(reply.size() > 0) {
				CSVOutput.generateOutput(reply);
			} else {
				logger.info("No data recived in reply");
			}
		
		} catch(Exception e) {
			logger.error("Failed to process the request correctly" , e);
		}
	}
}

package goeuro.out;

import goeuro.common.Location;
import goeuro.config.AppConfig;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CSVOutput {
	private static final Logger logger = LogManager.getLogger(CSVOutput.class.getName());
	private static final String CSV_SEPARATOR = ",";
	public static void generateOutput(List<Location> input) throws Exception {
		logger.info("Starting to generate output");
		String outputFileName = AppConfig.getConfig(AppConfig.CSV_OUTPUT_FILE_NAME);
		logger.debug("CSV output file name: " +outputFileName );
		try (
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFileName)));
				) {
			
			for (Location loc: input) {
				StringBuffer oneLine = new StringBuffer();
				oneLine.append(loc.getId());
				oneLine.append(CSV_SEPARATOR);
				
				oneLine.append(loc.getName());
				oneLine.append(CSV_SEPARATOR);
				
				oneLine.append(loc.getType());
				oneLine.append(CSV_SEPARATOR);
				
				oneLine.append(loc.getLatitude());
				oneLine.append(CSV_SEPARATOR);
				
				oneLine.append(loc.getLongitude());
				
				bw.write(oneLine.toString());
                bw.newLine();
			}
			bw.flush();
			logger.info("Writing to output file complet");
		}catch (Exception e) {
			logger.error("Failed to write to output file" , e);
			throw e;
		}
		
		
	}

}

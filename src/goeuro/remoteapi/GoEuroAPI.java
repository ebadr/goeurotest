package goeuro.remoteapi;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import goeuro.common.Location;
import goeuro.config.AppConfig;
import goeuro.parsers.GoEuroAPIParser;

public class GoEuroAPI {
	private static final Logger logger = LogManager.getLogger(GoEuroAPI.class.getName());
	
	public static List<Location> getReply (String input) throws Exception{
		String remoteURL = AppConfig.getConfig(AppConfig.API_URL) + input;
		logger.debug("Connecting to remote URL" + remoteURL);
		URL url = new URL(remoteURL);
		try (
				InputStream is = url.openStream();
				JsonReader rdr = Json.createReader(is);
				)
		{
			 JsonArray data = rdr.readArray();
			 logger.debug("reply has been correctly retrived in Json format");
			 logger.debug(data);
			 
			 return GoEuroAPIParser.parse(data);
			 
		} catch (Exception e) {
			logger.error("Error while getting reply from remote API: " + e.getMessage() , e);
			//logger.(e);
			throw e;
			
		}
	}

}

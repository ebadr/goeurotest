package goeuro;

import goeuro.config.AppConfig;
import goeuro.parsers.ComandLineParser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AppMain {
	 private static final Logger logger = LogManager.getLogger(AppMain.class.getName());

	public static void main(String[] args) {
		logger.info("App is starting");
		try {
			AppConfig.loadConfig();
			String parsedInput = ComandLineParser.Parse(args);
			if( parsedInput != null) {
				Processor.process(parsedInput);
			} else {
				logger.info("No Valid input found");
			}
		} catch (Exception e) {
			logger.error ("The app failed due to exception: " + e.getMessage() , e);

		}
		
		finally {
			logger.info("App is closing");
		}

	}

}

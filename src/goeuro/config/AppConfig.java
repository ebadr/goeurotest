package goeuro.config;



import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AppConfig {
	
	private static final Logger logger = LogManager.getLogger(AppConfig.class.getName());
	
	private static String propFileName = "config.properties";
	private static Properties prop = new Properties(); 
	
	public static final String API_URL= "goeuro.api.url";
	public static final String CSV_OUTPUT_FILE_NAME = "csvfile";
	
	public static void loadConfig() throws IOException {
		
		logger.info("Loading app configerations");
		
		
		logger.debug("loading property file: " + propFileName);
		InputStream inputStream = AppConfig.class.getClassLoader().getResourceAsStream(propFileName);
 
		if (inputStream != null) {
			prop.load(inputStream);
		} else {
			logger.error("Failed to load property file: " + propFileName);
			throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
		}
	}
	
	public static String getConfig (String key) {
		return prop.getProperty(key);
	}

}

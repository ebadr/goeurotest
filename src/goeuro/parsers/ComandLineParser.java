package goeuro.parsers;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ComandLineParser {
	private static final Logger logger = LogManager.getLogger(ComandLineParser.class.getName());
	public static String Parse(String[] input) {
		logger.debug("parsing input: " + input);
		String result = null;
		if(input != null && input.length > 0) {
			result = input[0];
		}
		logger.debug("Parse result: " + result);
		return result;
		
	}

}

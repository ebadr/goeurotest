package goeuro.parsers;

import goeuro.common.Location;

import java.util.ArrayList;
import java.util.List;

import javax.json.JsonArray;
import javax.json.JsonObject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GoEuroAPIParser {
	private static final Logger logger = LogManager.getLogger(GoEuroAPIParser.class.getName());
	public static List<Location> parse(JsonArray input) throws Exception{
		List<Location> result = new ArrayList<Location>();
		logger.debug("Parsing data");
		try{
			for (JsonObject obj : input.getValuesAs(JsonObject.class)) { 
				Location loc = new Location();
				loc.setId(obj.getJsonNumber("_id").toString());
				loc.setName(obj.getString("name"));
				loc.setType(obj.getString("type"));
				loc.setLongitude(obj.getJsonObject("geo_position").getJsonNumber("longitude").toString());
				loc.setLatitude(obj.getJsonObject("geo_position").getJsonNumber("latitude").toString());
				result.add(loc);
				
			}
		} catch (Exception e ) {
			logger.error("Error Parsing data: " + e.getMessage() , e);
			throw e;
		}
		logger.debug("Parse complete");
		return result;
	}
}
